﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;
using RSSFeed.Model;
using System.Windows;
using System.Xml;
using System.ServiceModel.Syndication;
using SQLite;
using System.Net;
using RSSFeed.Services;

namespace RSSFeed.ViewModel
{
    class MainWindowViewModel : BaseViewModel
    {
        #region Properties and Commands

        public ObservableCollection<RssSource> rssSources;
        public ObservableCollection<RssSource> RssSources
        {
            get { return rssSources; }
            set
            {
                rssSources = value;
                NotifyPropertyChanged("RssSources");
            }
        }

        private List<RssPost> rssPosts;
        public List<RssPost> RssPosts
        {
            get { return rssPosts; }
            set
            {
                rssPosts = value;
                NotifyPropertyChanged("RssPosts");
            }
        }

        private RelayCommand rssLoad;
        public ICommand RssLoad
        {
            get
            {
                return rssLoad ?? (rssLoad = new RelayCommand(param => LoadRss(param)));
            }
        }

        private RelayCommand addSubscription;
        public ICommand AddSubscription
        {
            get
            {
                return addSubscription ?? (addSubscription = new RelayCommand(param => AddRssSource()));
            }
        }

        private string rssTitle;
        public string RssTitle
        {
            get { return rssTitle; }
            set
            {
                rssTitle = value;
                NotifyPropertyChanged("RssTitle");
            }
        }

        private string rssUrl;
        public string RssUrl
        {
            get { return rssUrl; }
            set
            {
                rssUrl = value;
                NotifyPropertyChanged("RssUrl");
            }
        }

        private bool isDialogOpen;
        public bool IsDialogOpen
        {
            get { return isDialogOpen; }
            set
            {
                isDialogOpen = value;
                NotifyPropertyChanged("IsDialogOpen");
            }
        }

        private string dialogMessage;
        public string DialogMessage
        {
            get { return dialogMessage; }
            set
            {
                dialogMessage = value;
                NotifyPropertyChanged("DialogMessage");
            }
        }

        #endregion

        public MainWindowViewModel()
        {
            rssSources = new ObservableCollection<RssSource>();
            rssPosts = new List<RssPost>();
            IsDialogOpen = false;

            bool isInitialized = false;
            try
            {
                DatabaseService.Initialize();
                isInitialized = true;
            }
            catch
            {
                DialogMessage = "Failed to initalize";
                IsDialogOpen = true;
            }

            try
            {
                if (isInitialized)
                {
                    RssSources = new ObservableCollection<RssSource>(DatabaseService.LoadRssSources());
                }
            }
            catch
            {
                DialogMessage = "Failed to load RSS sources";
                IsDialogOpen = true;
            }
            
        }

        #region Command methods
        /// <summary>
        /// Загрузка ленты из заданного источника
        /// </summary>
        /// <param name="param">Источник RSS ленты</param>
        private void LoadRss(object param)
        {
            RssSource rssSrc = param as RssSource;
            if (rssSrc == null)
            {
                DialogMessage = "Error in application!";
                IsDialogOpen = true;
                return;
            }

            string rssUrl = rssSrc.Url;
            List<RssPost> posts = new List<RssPost>();
            bool isLoaded = false;
            try
            {
                using (XmlReader reader = XmlReader.Create(rssUrl))
                {
                    SyndicationFeed feed = SyndicationFeed.Load(reader);
                    RssPosts = feed.Items.Select(p => new RssPost { RssPostId = rssSrc.Id, Summary = p.Summary.Text, Title = p.Title.Text }).ToList();
                }
                isLoaded = true;
            }
            catch(WebException) //Оффлайн режим
            {
                LoadFromCache(rssSrc.Id);
            }
            catch
            {
                DialogMessage = "Failed to load RSS feed";
                IsDialogOpen = true;
            }

            if (isLoaded) //Сохранение постов
            {
                try 
                {
                    DatabaseService.SavePosts(RssPosts, rssSrc.Id);
                }
                catch 
                {
                    DialogMessage = "Failed to save RSS feed";
                    IsDialogOpen = true;
                }
            }
        }

        /// <summary>
        /// Добавление RSS источника
        /// </summary>
        private void AddRssSource()
        {
            if (String.IsNullOrEmpty(RssTitle) || String.IsNullOrEmpty(RssUrl))
            {
                DialogMessage = "RSS Title and RSS Url must be filled";
                IsDialogOpen = true;
                return;
            }
            
            //TODO validate input params

            RssSource rssSrc = new RssSource
            {
                Title = RssTitle,
                Url = RssUrl
            };

            try
            {
                DatabaseService.SaveRssSource(rssSrc);
                RssSources.Add(rssSrc);
            }
            catch
            {
                DialogMessage = "Can not save rss source";
                IsDialogOpen = true;
            }
            finally
            {
                RssTitle = String.Empty;
                RssUrl = String.Empty;
            }
        }
        #endregion

        /// <summary>
        /// Поддержка оффлайн режима.
        /// Загружает новости для заданного источника
        /// </summary>
        /// <param name="rssSrcId">ID источника</param>
        private void LoadFromCache(int rssSrcId)
        {
            try
            {
                RssPosts = DatabaseService.LoadPosts(rssSrcId);
            }
            catch
            {
                DialogMessage = "Can not load posts from cache";
                IsDialogOpen = true;
            }
        }
    }
}
