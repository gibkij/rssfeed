﻿using RSSFeed.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RSSFeed.Services
{
    static class DatabaseService
    {
        static readonly string dbname = "database";

        static public void Initialize()
        {
            using (var db = new SQLiteConnection(dbname))
            {
                db.CreateTable<RssPost>();
                db.CreateTable<RssSource>();
                var sources = db.Table<RssSource>();
                if (sources.Count() == 0)
                {
                    db.InsertAll(GetDefaultRssSourses());
                }
            }
        }

        static public List<RssPost> LoadPosts(int rssSourceId)
        {
            List<RssPost> result = new List<RssPost>();
            using (var db = new SQLiteConnection(dbname))
            {
                result = db.Table<RssPost>().Where(p => p.RssPostId == rssSourceId).ToList();
            }
            return result;
        }

        static public void SavePosts(List<RssPost> posts, int rssSourceId)
        {
            using (var db = new SQLiteConnection(dbname))
            {
                db.RunInTransaction(() =>
                {
                    db.Execute("DELETE FROM RssPost WHERE RssPostId = ?", new object[1] { rssSourceId });
                    db.InsertAll(posts);
                });
            }
        }

        static public void SaveRssSource(RssSource src)
        {
            using (var db = new SQLiteConnection(dbname))
            {
                db.Insert(src);
            }
        }

        static public List<RssSource> LoadRssSources()
        {
            List<RssSource> result = new List<RssSource>();
            using (var db = new SQLiteConnection(dbname))
            {
                result = db.Table<RssSource>().ToList();
            }
            return result;
        }

        static public List<RssSource> GetDefaultRssSourses()
        {
            return new List<RssSource>
            {
                new RssSource{Id = 1, Title = "Habrahabr - Best of day", Url = @"https://habrahabr.ru/rss/best/"},
                new RssSource{Id = 2, Title = "Habrahabr - Best of week", Url = @"https://habrahabr.ru/rss/best/weekly/"},
                new RssSource{Id = 3, Title = "Habrahabr - Best of month", Url = @"https://habrahabr.ru/rss/best/monthly/"},
                new RssSource{Id = 4, Title = "Habrahabr - Best of all time", Url = @"https://habrahabr.ru/rss/best/alltime/"}
            };
        }
    }
}
