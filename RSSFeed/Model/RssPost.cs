﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSSFeed.Model
{
    class RssPost
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int RssPostId { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
    }
}
